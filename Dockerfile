FROM alpine:3.12.0
# your jdk installiation
#helpers
RUN apk add git
RUN apk add bash
RUN apk add curl
RUN apk add jq
#copy semver script
COPY semver.sh .
#create shortcut -> "semver"
RUN mv semver.sh /usr/bin/semver && \
    chmod +x /usr/bin/semver

ARG USER_HOME_DIR="/app"

WORKDIR $USER_HOME_DIR

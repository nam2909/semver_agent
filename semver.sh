#!/bin/bash

VERSION=""

NAT='0|[1-9][0-9]*'
SEMVER_REGEX="\
^[vV]?\
($NAT)\\.($NAT)\\.($NAT)"

function validate-version() {
  local version=$1

  if [[ "$version" =~ $SEMVER_REGEX ]]; then
    # if a second argument is passed, store the result in var named by $2
    if [ "$#" -eq "2" ]; then
      local major=${BASH_REMATCH[1]}
      local minor=${BASH_REMATCH[2]}
      local patch=${BASH_REMATCH[3]}
      eval "$2=(\"$major\" \"$minor\" \"$patch\")"
    else
      echo "$version"
    fi
  else
    echo "regex error"
  fi
}

function bump() {
  local new
  local version
  local command

  case $# in
  2) case $1 in
    major | minor | patch)
      command=$1
      version=$2
      ;;
    *) echo "major minor or patch" ;;
    esac ;;
  *) echo "unknown command" ;;
  esac

  validate-version "$version" parts
  # shellcheck disable=SC2154

  local major="${parts[0]}"
  local minor="${parts[1]}"
  local patch="${parts[2]}"

  case "$command" in
  major) new="$((major + 1)).0.0" ;;
  minor) new="${major}.$((minor + 1)).0" ;;
  patch) new="${major}.${minor}.$((patch + 1))" ;;
  *) echo "unknown command major minor or patch" ;;
  esac

  VERSION="$new"
}

function tag_push() {
  echo "Tag Push Starting..."
  git config user.email "${GITLAB_USER_EMAIL}"
  git config user.name "${GITLAB_USER_NAME}"
  git remote add tag-origin https://oauth2:"${GITLAB_API_TOKEN}"@gitlab.com/"${CI_PROJECT_PATH}"
  git tag -a "$1" -m "Tag Created Automatically"
  git push tag-origin "$1" -o ci.skip
  echo "Git Tag created successfully"
  echo "Tag Push finished"
}


echo "Script Starting..."
CREATE_VERSION=false

git remote set-url origin "https://oauth2:${GITLAB_API_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"
git config user.name "$GITLAB_USER_NAME"
git config user.email "$GITLAB_USER_EMAIL"
git fetch || exit 1

BRANCH_NAME=$CI_COMMIT_BRANCH
echo "BRANCH_NAME:""$BRANCH_NAME"

echo "Tagging Starting..."
VERSION_BASE=$(git tag --list | tail -1 | cut -d'v' -f2)
echo "VERSION_BASE:""$VERSION_BASE"

if [[ -z $VERSION_BASE ]]; then
  echo "Init version"
  VERSION=1.0.0
elif [[ $DOCKER_ENV_CI_COMMIT_TITLE == "feat"* ]]; then
  echo "Creating Minor"
  bump minor "$VERSION_BASE"
else
  echo "Patching"
  bump patch "$VERSION_BASE"

fi
tag_push "$VERSION"

echo "Tagging Finished"


echo "VERSION:""$VERSION"
echo "CREATE_VERSION:""$CREATE_VERSION"

echo "$VERSION" >version
echo "Script Finished"
